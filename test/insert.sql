
-- The Coalesce is used only for starting the sequnce, after this first execution, detault should be use

INSERT INTO im_ok_normal_table default values;
INSERT INTO im_ok_but_two_serials default values;
INSERT INTO im_ok_but_im_not_published default values;
INSERT INTO im_ok_fancy_auto_generated DEFAULT VALUES;

INSERT INTO im_ok_no_serial
    SELECT coalesce((select max(i)+1 from im_ok_no_serial),1);

INSERT INTO im_not_ok_lo(f)
    SELECT lo_creat(-1);

INSERT INTO im_not_ok_lo(f)
    SELECT lo_from_bytea(0,random()::text::bytea);


INSERT INTO im_not_ok_lo_with_oids(f)
    SELECT lo_creat(-1);

INSERT INTO im_not_ok_lo_with_oids(f)
    SELECT lo_from_bytea(0,random()::text::bytea); 
    -- 0 is to let postgres assign oid, you can assign an oid manually

INSERT INTO im_not_ok_no_replica_identity default values;




