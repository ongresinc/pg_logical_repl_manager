CREATE OR REPLACE FUNCTION warning_ddl()
  RETURNS event_trigger
 LANGUAGE plpgsql
  AS $$
BEGIN
  RAISE EXCEPTION 'Alter are disabled when LR is enabled. % is disabled', tg_tag;
END;
$$;

CREATE EVENT TRIGGER anti_ddl_{{table_oid}}
ON ddl_command_start
   EXECUTE PROCEDURE warning_ddl();