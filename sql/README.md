## 

[Source](https://www.postgresql.org/docs/10/static/logical-replication-restrictions.html)

Logical replication currently has the following restrictions or missing functionality. 
These might be addressed in future releases.

- The database schema and DDL commands are not replicated. 
  The initial schema can be copied by hand using pg_dump --schema-only. 
  Subsequent schema changes would need to be kept in sync manually. (Note, however, 
  that there is no need for the schemas to be absolutely the same on both sides.) 
  Logical replication is robust when schema definitions change in a live database: 
  When the schema is changed on the publisher and replicated data starts arriving 
  at the subscriber but does not fit into the table schema, replication will error 
  until the schema is updated. In many cases, intermittent errors can be avoided by 
  applying additive schema changes to the subscriber first.


```sql
CREATE OR REPLACE FUNCTION warning_ddl()
  RETURNS event_trigger
 LANGUAGE plpgsql
  AS $$
BEGIN
  RAISE EXCEPTION 'Alter are disabled when LR is enabled. % is disabled', tg_tag;
END;
$$;

CREATE EVENT TRIGGER anti_ddl_<table> 
ON ddl_command_start
   EXECUTE PROCEDURE warning_ddl();
```

- Sequence data is not replicated. The data in serial or identity columns backed 
by sequences will of course be replicated as part of the table, but the sequence 
itself would still show the start value on the subscriber. If the subscriber is 
used as a read-only database, then this should typically not be a problem. If, however, 
some kind of switchover or failover to the subscriber database is intended, then the 
sequences would need to be updated to the latest values, either by copying the current 
data from the publisher (perhaps using pg_dump) or by determining a sufficiently high 
value from the tables themselves.

- TRUNCATE commands are not replicated. This can, of course, be worked around by 
using DELETE instead. To avoid accidental TRUNCATE invocations, you can revoke 
the TRUNCATE privilege from tables.


```sql
CREATE TRIGGER anti_truncate
  INSTEAD OF TRUNCATE ON <table>
  FOR EACH STATEMENT
  EXECUTE PROCEDURE warning_message();
```


- Large objects (see Chapter 34) are not replicated. There is no workaround for 
that, other than storing data in normal tables.

- Replication is only possible from base tables to base tables. That is, the 
tables on the publication and on the subscription side must be normal tables, 
not views, materialized views, partition root tables, or foreign tables. 
In the case of partitions, you can therefore replicate a partition hierarchy one-to-one,
but you cannot currently replicate to a differently partitioned setup. Attempts to 
replicate tables other than base tables will result in an error.


## Identities

Identity support is not yet tested, there should no need for updating sequences in 
this case, although it might not be _compatible_ with Logical Replication (?).

https://blog.2ndquadrant.com/postgresql-10-identity-columns/

```
CREATE OR REPLACE FUNCTION upgrade_serial_to_identity(tbl regclass, col name)
RETURNS void
LANGUAGE plpgsql
AS $$
DECLARE
  colnum smallint;
  seqid oid;
  count int;
BEGIN
  -- find column number
  SELECT attnum INTO colnum FROM pg_attribute WHERE attrelid = tbl AND attname = col;
  IF NOT FOUND THEN
    RAISE EXCEPTION 'column does not exist';
  END IF;

  -- find sequence
  SELECT INTO seqid objid
    FROM pg_depend
    WHERE (refclassid, refobjid, refobjsubid) = ('pg_class'::regclass, tbl, colnum)
      AND classid = 'pg_class'::regclass AND objsubid = 0
      AND deptype = 'a';

  GET DIAGNOSTICS count = ROW_COUNT;
  IF count < 1 THEN
    RAISE EXCEPTION 'no linked sequence found';
  ELSIF count > 1 THEN
    RAISE EXCEPTION 'more than one linked sequence found';
  END IF;  

  -- drop the default
  EXECUTE 'ALTER TABLE ' || tbl || ' ALTER COLUMN ' || quote_ident(col) || ' DROP DEFAULT';

  -- change the dependency between column and sequence to internal
  UPDATE pg_depend
    SET deptype = 'i'
    WHERE (classid, objid, objsubid) = ('pg_class'::regclass, seqid, 0)
      AND deptype = 'a';

  -- mark the column as identity column
  UPDATE pg_attribute
    SET attidentity = 'd'
    WHERE attrelid = tbl
      AND attname = col;
END;
$$;
```