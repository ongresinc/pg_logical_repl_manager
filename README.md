# pg_logical_repl_manager

Sorry, this project is no longer maintained! We're working in a new implementation
called [eva00](https://gitlab.com/ongresinc/eva00), stay tuned!

Although, some things that this projects handlest well:

- Initial setup of your LR definition. By running `report` you can get all needed
  information to avoid errors on production, on tables defined with publish events
  that aren't supported due to replica identity indefinitions. 
- There are plenty of hacks and things that you can reuse, as we found Logical Replication 
  with a lack of checks in the setup that can turn into applications stop working.


## What this does.

It is a publication/subscription manager, in order to deploy Logical Replication
easily.

You can create multiple pub subs in different servers (we need to improve
the connection part, so you don't have to store pass).

## Basic usage

Vault file password isn't mandatory unless you have valut entries in your 
yaml.

`-c` is mandatory in most of the actions. **USE FULL PATH**.

```
./lrmanager [action] -v $HOME/lrm_vault -c ./.config.yaml
```

## Prerequisites

- A user allowed to access both replication and the source/dest database.
  (This includes creating entries in the pg_hba.conf)

## YAML Conf

### Connections Section

For more information about the available options in the connection string, [See libpq-connstring](https://www.postgresql.org/docs/10/static/libpq-connect.html#LIBPQ-CONNSTRING).

### Publications Section

### Subscriptions Section

```
HINT:  Replication slot names may only contain lower case letters, numbers, and the underscore character.
```




## Prerequisites

On Debian based distros:

```
apt-get install python3-venv
```

