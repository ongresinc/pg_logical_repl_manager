#!/bin/bash

[[ -z $(docker-compose ps -q ) ]] && docker-compose up -d

docker-compose down


echo "Configuring"

for pp in $(ls .data)
do
    echo -e "\n#added auto\ninclude = 'pglogical.conf'\n" >> .data/${pp}/postgresql.conf 


    cat > .data/${pp}/pglogical.conf <<_EOF
    wal_level=logical
    max_wal_senders=20
    max_replication_slots=20
    max_logical_replication_workers=20
    max_sync_workers_per_subscription=2
    hot_standby = on
_EOF
    echo -e "\nhost replication all all trust\nhost all all all trust\n" >> .data/${pp}/pg_hba.conf

done


echo "Refreshing things up"
docker-compose up -d


## Uncomment if you want to spin a replica container
# sleep 3
# echo "starting replica"
# docker-compose stop postgres_replica && rm -rf .data/pg_replica
# # dbname is the connection string, not the database name
# # Using --slot="replica" needs a pre-existintent slot,  does not create
# pg_basebackup -h localhost -p 15555 -U postgres \
#   --checkpoint="spread" --label="replica"  \
#   --write-recovery-conf --progress --no-sync -D .data/pg_replica 

# sleep 10  
# docker-compose start postgres_replica