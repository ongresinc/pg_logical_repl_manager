#!/usr/bin/python
import os 
import configYaml
from servers import *
from publications import *
from subscriptions import *
# from installFunctions import min_supported_version
from ansible_vault import Vault
from errors import *
import socket
from termcolor import colored, cprint
min_supported_version='10' # Support for 9.x is still WIP

# Text colors:    # Text highlights:
#         grey    #         on_grey
#         red     #         on_red
#         green   #         on_green
#         yellow  #         on_yellow
#         blue    #         on_blue
#         magenta #         on_magenta
#         cyan    #         on_cyan
#         white   #         on_white

# Attributes:
#         bold
#         dark
#         underline
#         blink
#         reverse
#         concealed



def qxprint(text, levelMess='normal'):
    preTextLevels = {
        'section': '\n# ',
        'subsection': '\n## ',
        'error': 'ERROR: ',
        'warning': 'Warning: ',
        'normal': '\t\t',
        'connection': '\nConnection: ',
        'header': '\t\t',
        'item': '* ',
    }
    levelC = {
        'section':  'cyan',
        'subsection': 'cyan',
        'error':    'red',
        'warning':  'yellow',
        'normal': 'white',
        'connection': 'blue',
        'header': 'white',
        'item': 'white',
    }
    levelHL ={
        'section': '',
        'subsection': '',
        'error': '',
        'warning': '',
        'normal': '',
        'connection': '',
        'header': 'on_grey',
        'item': '',
    }
    levelattr ={
        'section': ['bold'],
        'subsection': [],
        'header': ['bold'],
        'subsection': [],
        'error': [],
        'warning': [],
        'normal': [],
        'connection': ['bold'],
        'item': [],
    }
    postTextLevels = {
        'section': '\n',
        'subsection': '\n',
        'error': '',
        'warning': '',
        'normal': '',
        'connection': '',
        'header': '',
        'item': '',
    }

    if isinstance(text,list):
        for i in text:
            cprint(preTextLevels[levelMess] + str(i) + postTextLevels[levelMess], 
                    levelC[levelMess], 
                    levelHL[levelMess] if levelHL[levelMess] != '' else None, 
                    attrs=levelattr[levelMess])
    else:
        cprint(preTextLevels[levelMess] + text + postTextLevels[levelMess], 
            levelC[levelMess], 
            levelHL[levelMess] if levelHL[levelMess] != '' else None, 
            attrs=levelattr[levelMess])

def hErrNum(message):
    return errors[message]

def hErrMessage(message, errormsg=""):
    builtStr = errorsDesc[errors[message]] + " - " + (str(errormsg) if str(errormsg) != "" else "")
    qxprint(builtStr,'error')
    return errors[message]


def checkBooleanValues(value):
    if value is None or value == "":
        raise ValueError("Value is empty")
    if value is True:
        return "true"
    if value is False:
        return "false"
    
    pvalue = value.lower().trim().replace("'",'')
    if not pvalue == "true" or not pvalue != "false":
        raise ValueError("The value is not boolean")

    return pvalue

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def getHba(serverInfo):
    
    hba = """
    host    replication     {1}     {2}/32  md5
    hostssl replication     {1}     {2}/32  md5
    host    {0}         {1}         {2}/32  md5
    hostssl {0}         {1}         {2}/32  md5
    """.format( serverInfo["dbname"],
                serverInfo["user"],
                get_ip()) # TODO handle to provide the user and db 
    return hba

def elog(args, message):
    if args.debug:
        print(message) # print message with debug info
    else:
        a =1 # Print no debug message
    return True

def getVaultP(pfile):
    """
    getVaultP
    Reads password from file
    """

    if "LRM_VAULT" in os.environ:
        env_vault = os.environ['LRM_VAULT']

    if pfile is None or pfile == False or pfile == "" and (env_vault == "" or env_vault is None):
        print("Missing vault file. Use -v or use LRM_VAULT")
        sys.exit(2)
        
    try:
        pfile = pfile if pfile != "" else env_vault
        p = open(pfile,'r')
        vaultp = p.read()
        p.close()
    except Exception as e:
        sys.exit(hErrMessage('ERROR_READ_VAULT_PASS_FILE', e))
    
    return vaultp

class ExtendedVault(Vault):
    def load_raw(self, stream):
        return self.vault.decrypt(stream)
    def dump_raw(self, text, stream=None):
        encrypted = self.vault.encrypt(text)
        if stream:
            # f = open(stream,'w')
            stream.write(encrypted)
        else:
            return encrypted

def getServers(args, yamlConf):
    #Instantiate servers with connections and install if install flag is enabled
    servers = {}
    connections = configYaml.getConnections(args,yamlConf)
    conndetails = configYaml.getConnDetails(args,yamlConf)
    for serverName,connString in connections.items():
        if args.debug:
            print("getServers",serverName,connString,conndetails[serverName])
        servers[serverName] = Server(serverName,
                                    conndetails[serverName],
                                    connString, min_supported_version, True if args.action=="install" else False )
    
    return servers

def getPubDef(args,yamlConf):
    publicationsRaw = configYaml.getPublications(yamlConf)
    publications = {}
    connections = configYaml.getConnections(args,yamlConf)
    for pubName,pubDetails in publicationsRaw.items():
        for conn in pubDetails['connections']:
            if conn in connections:
                publications[pubName] = Publication(pubName, pubDetails)
    return publications

def getPubs(args, yamlConf):
    publicationsRaw = configYaml.getPublications(yamlConf)
    publications = {} 
    servers = getServers(args,yamlConf)
    if publicationsRaw is None:
        return publications
    for pubName,pubDetails in publicationsRaw.items():
        for conn in pubDetails['connections']:
            if conn in servers:
                publications[pubName] = Publication(pubName, pubDetails)
    return publications

def getSubs(args, yamlConf):
    subscriptionsRaw = configYaml.getSubscriptions(yamlConf)
    subs = {} 
    if subscriptionsRaw is None:
        return subs
    for subName, subDetails in subscriptionsRaw.items():
        subs[subName] = Subscription(subName,subDetails)
    return subs


class Util():
    """
    Utils return queries to be executed into the servers.
    
    """
    def __init__(self):
        self.name = "utils"

    def genYamlPub(self,pubRow,pubConn):
        pubName = pubRow[0]
        pubTables = pubRow[1]
        pubPublish = pubRow[2]
        yaml ="""
        {0}:
            tables:
                - {1}
            publish:
                - {2}
            connections:
                - {3}
        """.format(pubName, pubTables,pubPublish,pubConn)
        return yaml 
    def genYamlSub(self,subRow,subConn):
        """
        ('prod_sync_sub', 16389, True, 'connstring', 'prod_sync_sub', 'off', ['fullpub'])
                subname,
                subowner,
                subenabled,
                subconninfo,
                subslotname,
                subsynccommit,
                subpublications

          mylocalmagicsub:
            # copy_data: true
            # create_slot: true
            # enabled: true
            # #slot_name: 'sarasa'
            # synchronous_commit: 'off'
            # connect: true 
            publications: 
            - mymagicpub
            connections:
            - serverlocalsub
        """
        subName = subRow[0]
        subEnabled = subRow[2]
        subSlotName = subRow[4]
        subSyncCommit = subRow[5]
        subPubs = subRow[6]
        subConn = subConn

        yaml = """
        {0}:
            copy_data: ??
            slot_name: {1}
            enabled: {2}
            synchronous_commit: '{3}'
            connect: true 
            publications:
                - {4}
            connections:
                - {5}

        """.format(subName,subSlotName,subEnabled,subSyncCommit, subPubs,subConn)
        return yaml

    def getSettingsSQL(self,settingCategory):
        sql = """
        SELECT  name,
                setting,
                short_desc::text , 
                extra_desc::text, 
                context,
                sourcefile || '#L' || sourceline as varloc
        FROM pg_settings
        WHERE 
            category ~ '^{0}'
        ORDER BY context
        """.format(settingCategory)
        return sql
    def getSendersStatus(self):
        sql = """
        select 
            slot_name,
            plugin,
            slot_type,
            temporary,
            active,
            active_pid,
            xmin,
            catalog_xmin,
            restart_lsn,
            confirmed_flush_lsn,
            client_addr, client_hostname, client_port,
            backend_start,
            state_change,
            wait_event_type,
            wait_event,
            state,
            backend_type
            from pg_replication_slots prs left outer join pg_stat_activity psa 
                on (psa.pid = prs.active_pid);
        """
        return sql
    def getApplierStatus(self):

        sql = """
        select 
            count(*) over () num_workers,
            subid                 ,
            subname               ,
            pss.pid                   ,
            relid                 ,
            received_lsn          ,
            last_msg_send_time    ,
            last_msg_receipt_time ,
            latest_end_lsn        ,
            latest_end_time       ,
            -- psa
            datid                 ,
            backend_start         ,
            xact_start            ,
            query_start           ,
            state_change          ,
            wait_event_type       ,
            wait_event            ,
            state                 ,
            backend_xid           ,
            backend_xmin          ,
            query                 ,
            backend_type          
            from
                pg_stat_subscription pss left outer join
                pg_stat_activity psa 
                on (psa.pid = pss.pid)
        """
        return sql
    def getSubStatus(self,subName):
        sql = """
        SELECT subid,subname,relid,received_lsn,
            last_msg_send_time,last_msg_receipt_time,
            latest_end_lsn,latest_end_time
        FROM pg_stat_subscription
        WHERE subname = '{0}'
        """.format(subName)
        return sql

    def getSlotsStatus(self):
        sql = """
        SELECT 
            slot_name   ,
            plugin              ,
            slot_type           ,
            datoid              ,
            database            ,
            temporary           ,
            active              ,
            active_pid          ,
            xmin                ,
            catalog_xmin        ,
            restart_lsn         ,
            confirmed_flush_lsn 
        FROM pg_replication_slots
        """       
        return sql
    def getPubStatus(self,slotname):
        """
        getPuStatus
        requires slotname, as it returns the status of the
        socket. sub.slotName() returns this
        """
        sql = getSlotStatus() + """
        WHERE slot_name = '{0}'
        """.format(slotname)
        return sql

    def getLRSubSettings(self):
        return self.getSettingsSQL("Replication / Subscribers")
    def getLRSendServSettings(self):
        return self.getSettingsSQL("Replication / Sending Servers")
    def getLastValueSeq(self,table,schema='public'):
        sql = """
        SELECT lrmanager.last_value_from_table(getRegclass('{0}','{1}')
        """.format(schema,table)
        return sql
    def lrmversion(self):
        sql = """
        SELECT lrmanager.lrmanager_version();
        """
        return sql
    def versionIsSupported(self,min_supported_version):
        sql = """
        select (regexp_match(version(),'[PostgreSQL]\s(\d+)'))[1]::int >= {0}
        """.format(min_supported_version)
        return sql
    def checkAllTables96(self):
        """
        Uses installed toolkit.

        This needs to return: sequence check, identities check (see installFunctions.py)
        """
        return True

    def publishDefault(self,pubPublish):
        if pubPublish == ""  or pubPublish is None:
            pubPublish = "insert,update,delete"
        return pubPublish
    def getCreatePubAllTablesSQL(self,pubName,pubPublish=""):
        sql = """
        CREATE PUBLICATION {0} FOR ALL TABLES 
        WITH ( publish = '{1}')
        """.format(pubName,self.publishDefault(pubPublish))
        return sql
    def getCreatePubExplicitTablesSQL(self, pubName, pubTables, pubPublish=""):
        sql = """
        CREATE PUBLICATION {0} FOR TABLE {1} 
        WITH ( publish = '{2}')
        """.format(pubName,",".join(pubTables),self.publishDefault(pubPublish))
        return sql

    def getTableCreation(self,tablename):
        # https://stackoverflow.com/a/42742509/3264121
        sql = """
        with pkey as
        (
            select cc.conrelid, format(E',
            constraint %I primary key(%s)', cc.conname,
                string_agg(a.attname, ', ' 
                    order by array_position(cc.conkey, a.attnum))) pkey
            from pg_catalog.pg_constraint cc
                join pg_catalog.pg_class c on c.oid = cc.conrelid
                join pg_catalog.pg_attribute a on a.attrelid = cc.conrelid 
                    and a.attnum = any(cc.conkey)
            where cc.contype = 'p'
            group by cc.conrelid, cc.conname
        )
        select format(E'create %stable %s%I\n(\n%s%s\n);\n',
            case c.relpersistence when 't' then 'temporary ' else '' end,
            case c.relpersistence when 't' then '' else n.nspname || '.' end,
            c.relname,
            string_agg(
                format(E'\t%I %s%s',
                    a.attname,
                    pg_catalog.format_type(a.atttypid, a.atttypmod),
                    case when a.attnotnull then ' not null' else '' end
                ), E',\n'
                order by a.attnum
            ),
            (select pkey from pkey where pkey.conrelid = c.oid)) as sql
        from pg_catalog.pg_class c
            join pg_catalog.pg_namespace n on n.oid = c.relnamespace
            join pg_catalog.pg_attribute a on a.attrelid = c.oid and a.attnum > 0
            join pg_catalog.pg_type t on a.atttypid = t.oid
        where c.relname = '{0}'
        group by c.oid, c.relname, c.relpersistence, n.nspname;

        """.format(tablename)
        return sql
    def checkSingleTable(self, tablename):
        sql = self.checkAllTables() + """
        WHERE
            at.relid::regclass = '{0}'::regclass 
        """.format(tablename)
        return sql 
    def checkAllTables(self):
        """
        This is for ppl that don't want to spend much time configuring the yaml.
        This just checks connection(s)
        """

        sql = """
        WITH all_tables AS (
          select (schemaname||'.'||tablename)::regclass relid
            from pg_tables 
            where schemaname not in ('information_schema','pg_catalog')
        ),
        potentialRepIdIx AS (
            select
                t.oid::regclass as table,
                i.relname as index_name,
                array_to_string(array_agg(a.attname), ', ') as column_names
            from
                pg_class t,
                pg_class i,
                pg_index ix,
                pg_attribute a
            where
                (ix.indisunique and indexprs is null) and
                t.oid = ix.indrelid
                and i.oid = ix.indexrelid
                and a.attrelid = t.oid
                and a.attnum = ANY(ix.indkey)
                and t.relkind = 'r'
                -- and t.oid::regclass = ''::regclass
            group by
                t.oid,
                i.relname
            order by
                t.oid,
                i.relname
        )
        SELECT at.relid,
            pg_relation_is_publishable(at.relid) is not false 
                and pg_get_replica_identity_index(relid) is not null all_events_pub, 
            pg_relation_is_publishable(at.relid) is_publishable,
            pg_get_replica_identity_index(relid) replica_idix,
            pri.index_name potential_repid_ix, -- 4
            pri.column_names potential_repid_cols -- 5
        FROM all_tables at(relid) 
           left outer join potentialRepIdIx pri 
           ON (pri.table = at.relid::regclass)
        """
        return sql
    
    def getPotentialRepIdIx(self, regclass):
        sql = """
        select
            t.relname as table_name,
            i.relname as index_name,
            array_to_string(array_agg(a.attname), ', ') as column_names
        from
            pg_class t,
            pg_class i,
            pg_index ix,
            pg_attribute a
        where
            (ix.indisunique and indexprs is null) and
            t.oid = ix.indrelid
            and i.oid = ix.indexrelid
            and a.attrelid = t.oid
            and a.attnum = ANY(ix.indkey)
            and t.relkind = 'r'
            and t.oid::regclass = {0}::regclass
            -- and t.relname like 'yourtable'
        group by
            t.relname,
            i.relname
        order by
            t.relname,
            i.relname;
        """.format(regclass)
        return sql

    def getSetRepIdSQL(self,tablename,repId):
        sql = """ALTER TABLE {0} REPLICA IDENTITY USING INDEX {1};""".format(tablename, repId)
        return sql

    def getDefinedSubs(self):
        sql = """
        SELECT  subname,
                subowner,
                subenabled,
                subconninfo,
                subslotname,
                subsynccommit,
                subpublications
        FROM pg_subscription
        """
        return sql

    def getDefinedPubs(self):
        sql = """
        WITH tablesGroupByPub AS(
            SELECT 
                pubname,
                string_agg(schemaname || '.' || tablename,',') as tables
                from pg_publication_tables 
                group by pubname
        )
        SELECT pubname,
                CASE 
                    WHEN puballtables = true 
                    THEN 'all'
                    ELSE 
                        (tp.tables)
                    END
                    as tables, 
                concat_ws(',',
                CASE WHEN pp.pubinsert = true THEN 'insert' END,
                CASE WHEN pp.pubupdate = true THEN 'update' END,
                CASE WHEN pp.pubdelete = true THEN 'delete' END
                ) as publish
        from pg_publication pp join tablesGroupByPub tp USING (pubname)
        """
        return sql

    def getRepIdFromPubTables(self, pubName):
        sql = """
        select pg_get_replica_identity_index(pp.relid)
        from pg_get_publication_tables('{0}') pp(relid)
        """.format(pubName)
        return sql
    def getRepIdFromTable(self, tablename):
        sql = """
        select pg_get_replica_identity_index('{0}'::regclass)
        """.format(tablename)
        return sql
        

    
    def toRegclass(self,table,schema='public'):
        return "('" + schema + "." + table + "')::regclass"

    def getLastValueSeqSQL(self,table,schema='public'):
        """

        get the sequence value
        There is a native function (pg_get_serial_sequence) but it
        takes both table and column, this hackish scans all columns at once.
        """
        sql = """
        WITH last_seq_val_table AS (
            select adrelid,last_value 
            FROM pg_sequences 
            where sequencename IN (
              select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
            from pg_attrdef 
            where adrelid = {0} and adsrc::text ~ 'nextval'
            )
        SELECT adrelid, last_value lvt FROM last_seq_val_table
        """.format(self.toRegclass(table,schema))
        return sql

    def setSequenceValueSQL(self,seqrelid,newval):
        sql = """
        SELECT setval('{0}'::regclass,{1});
        """.format(seqrelid,newval)
        return sql
    
    def getPubSeqOidLV(self, optpubname):
        sql = """
            select (schemaname || '.' || sequencename)::regclass::oid seqoid, last_value 
                FROM pg_sequences 
                where sequencename IN (
                select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
                from pg_attrdef 
                where adrelid IN (select * from pg_get_publication_tables('{0}'))
                     and adsrc::text ~ 'nextval'
                );
        """.format(optpubname)
        return sql 

    def getPubSequences(self, optpubname):
        sql = """
            select (schemaname || '.' || sequencename)::regclass seqname, last_value lvt
                FROM pg_sequences 
                where sequencename IN (
                select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
                from pg_attrdef 
                where adrelid IN (select * from pg_get_publication_tables('{0}'))
                     and adsrc::text ~ 'nextval'
                );
        """.format(optpubname)
        return sql 

    def getAllPubLastValSeqSQL(self, pubName):
        """
        getAllPubLastValSeq
        scans all tables in publication and return their sequences (identities
        is managed separately).
        """
        sql = """
        WITH map AS (
            select adrelid, 
                pg_get_replica_identity_index(adrelid::regclass) as rpidix,
                (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1] as seqname
            -- last_value lvt 
            from   pg_get_publication_tables('{0}') pt(adrelid) 
                        JOIN pg_attrdef pat USING (adrelid)
                        --JOIN pg_sequences ps ON 
            WHERE adsrc::text ~ 'nextval'
        )
        SELECT *, pg_sequence_last_value(seqname::regclass) as lvt FROM map;
        """.format(pubName)
        return sql # returns relid, lvt


    def getAllLastValSeq(self):
        """
        getAllLastValSeq
        scans all tables  and return their sequences (identities
        is managed separately).
        pg_sequence_last_value gets a regclass of the sequence, we
        should improve here much more
        """
        sql = """
        WITH all_tables AS (
          select (schemaname||'.'||tablename)::regclass relid
            from pg_tables 
            where schemaname not in ('information_schema','pg_catalog')
        )
        select sq.relid, sq.relid::regclass, last_value 
        FROM pg_sequences ps JOIN 
        (
            select pat.adrelid relid, 
                    (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1] sqname
            from pg_attrdef pat join all_tables at on (pat.adrelid = at.relid)
            where  adsrc::text ~ 'nextval'
        ) sq ON (ps.sequencename = sq.sqname)
        """
        return sql # returns relid, lvt

    