#!/usr/bin/python
# from utils import qxprint


"""
Ideally, we want to collect all the errors and build a list so
we can track them for better debugging.

"""

errors = {
    'OK': 0,
    'FAILED_PARSING_ARGS': 1,
    'VAULT_FAILED_DECRYPT': 2,
    'YAML_CONF_SUBS_ERROR': 3,
    'ERROR_READ_VAULT_PASS_FILE': 4,
    'TOO_MANY_PUBS_SYNCSEQ': 5,
    'ERR_QUERY_SEQS': 6,
    'FATAL_CONN_PG': 7,
    'FAIL_READ_CONN': 8,
    'TEST_CONN_FAIL': 9,
    'PUB_NOT_EXIST': 10,
    'INVALID_CHARS_IN_SUBNAME': 11,
    'NO_PUBS_SYNCSEQ': 12,
    'CONN_NOT_IN_YAML': 13,
    'qweqw1': 14,
    'qweqw2': 15,
    'qweqw3': 16,
    'qweqw4': 17,
    'qweqw5': 18,
    'qweqw6': 19,
} 

errorsDesc = {
    0: "OK.",
    1: "Error on argparse when parsing arguments. ",
    2: "Vault secret failed to decrypt. Use generate with your current vault file.",
    3: """
Something is misconfigured in the config.yaml at subscriptions
Values on boolean should be true or false. 
""",
    4: "Error reading vault password file. ",
    5: "Too many pubs, only 1 allowed when syncing sequences",
    6: "Error querying sequences",
    7: "Connection on server failed ",
    8: "Failed to read connection. ",
    9: "Testing connections failed. ",
   10: "Publication does not exist",
   11: "Replication slot names may only contain lower case letters, numbers, and the underscore character. If you want a Case-sensitive subscription, specify a slot_name in the configuration",
   12: "No publications in synsequence action were defined, use --pub and --conn", 
   13: "syncsequences custom --conn is not in yaml",
   14: "",
   15: "",
   16: "",
}

