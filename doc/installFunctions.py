#!/usr/bin/python
"""
DEPRECATED

"""

min_supported_version="10" # Support for 9.x is still WIP

funcs_lrmversion = "0.1"

sql_function_creation="""

CREATE SCHEMA IF NOT EXISTS lrmanager;

SET search_path TO lrmanager;

DROP FUNCTION IF EXISTS lrmanager_version();
CREATE OR REPLACE FUNCTION lrmanager_version() 
RETURNS text 
AS $lrmanager_version$
    select '{0}'; 
$lrmanager_version$ language sql IMMUTABLE ;

DROP FUNCTION IF EXISTS version_check(min_supp_version int);
CREATE OR REPLACE FUNCTION version_check(min_supp_version int) RETURNS boolean 
AS $version_check$
    select (regexp_match(version(),'[PostgreSQL]\s(\d+)'))[1]::int >= min_supp_version 
$version_check$ language sql IMMUTABLE ;

DROP FUNCTION IF EXISTS getOid(schemaname text,tablename text);
CREATE OR REPLACE FUNCTION getOid(schemaname text,tablename text) RETURNS oid 
AS $getOid$
    select to_regclass(schemaname || '.' || tablename)::regclass::oid; 
$getOid$ language sql IMMUTABLE ;

DROP FUNCTION IF EXISTS getRegclass(schemaname text,tablename text);
CREATE OR REPLACE FUNCTION getRegclass(schemaname text,tablename text) RETURNS regclass 
AS $getRegclass$
    select to_regclass(schemaname || '.' || tablename)::regclass; 
$getRegclass$ language sql IMMUTABLE ;


DROP FUNCTION IF EXISTS sequences_from_table(thetable regclass);
CREATE OR REPLACE FUNCTION sequences_from_table(thetable regclass)
RETURNS setof text AS $sequences_from_table$
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable and adsrc::text ~ 'nextval'
$sequences_from_table$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS sequences_from_table(thetable text);
CREATE OR REPLACE FUNCTION sequences_from_table(thetable text)
RETURNS setof text AS $sequences_from_table$
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable::regclass and adsrc::text ~ 'nextval'
$sequences_from_table$
IMMUTABLE LANGUAGE sql;


DROP FUNCTION IF EXISTS last_value_from_table(thetable text);
CREATE OR REPLACE FUNCTION last_value_from_table(thetable text)
RETURNS SETOF bigint AS $last_value_from_table$
select last_value 
FROM pg_sequences 
where sequencename IN (
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable::regclass and adsrc::text ~ 'nextval'
);
$last_value_from_table$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS last_value_from_table(thetable regclass);
CREATE OR REPLACE FUNCTION last_value_from_table(thetable regclass)
RETURNS SETOF bigint AS $last_value_from_table$
select last_value 
FROM pg_sequences 
where sequencename IN (
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable and adsrc::text ~ 'nextval'
);
$last_value_from_table$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS replica_identity_check(thetable regclass);
CREATE OR REPLACE FUNCTION replica_identity_check(thetable regclass)
RETURNS boolean AS $replica_identity_check$
Select count(*) > 0
FROM pg_attribute
Where attidentity is not null 
    and attidentity != ''
    and attrelid = thetable
$replica_identity_check$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS replica_identity_check(thetable text);
CREATE OR REPLACE FUNCTION replica_identity_check(thetable text)
RETURNS boolean AS $replica_identity_check$
Select count(*) > 0
FROM pg_attribute
Where attidentity is not null 
    and attidentity != ''
    and attrelid = thetable::regclass
$replica_identity_check$
IMMUTABLE LANGUAGE sql;

CREATE OR REPLACE FUNCTION warning_ddl()
  RETURNS event_trigger
 LANGUAGE plpgsql
  AS $warning_ddl$
BEGIN
  RAISE EXCEPTION 'Alter are disabled when LR is enabled. % disabled', tg_tag;
END;
$warning_ddl$;


DROP FUNCTION IF EXISTS pre_large_object_check(thetable regclass);
CREATE OR REPLACE FUNCTION pre_large_object_check(thetable regclass)
RETURNS boolean AS $pre_large_object_check$
 select count(*) = 0 
 from pg_attribute 
 where attrelid = thetable
    and attname NOT IN ('oid', 'tableoid') -- excluding system oid
    and atttypid::regtype = 'oid'::regtype ;
$pre_large_object_check$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS pre_large_object_check(thetable text);
CREATE OR REPLACE FUNCTION pre_large_object_check(thetable text)
RETURNS boolean AS $pre_large_object_check$
 select count(*) = 0 
 from pg_attribute 
 where attrelid = thetable::regclass
    and attname NOT IN ('oid', 'tableoid') -- excluding system oid
    and atttypid::regtype = 'oid'::regtype ;
$pre_large_object_check$
IMMUTABLE LANGUAGE sql;


DROP FUNCTION IF EXISTS get_all_tables();
CREATE OR REPLACE FUNCTION get_all_tables(OUT relid regclass)
RETURNS SETOF regclass AS $get_all_tables$
  select (schemaname||'.'||tablename)::regclass relid
  from pg_tables 
  where schemaname not in ('information_schema','pg_catalog');
$get_all_tables$
IMMUTABLE LANGUAGE sql;


DROP FUNCTION IF EXISTS get_all_tables_on_schema();
CREATE OR REPLACE FUNCTION get_all_tables_on_schema(theschema text)
RETURNS SETOF regclass AS $get_all_tables_on_schema$
  select (schemaname||'.'||tablename)::regclass 
  from pg_tables 
  where schemaname not in ('information_schema','pg_catalog')
    and schemaname = theschema;
$get_all_tables_on_schema$
IMMUTABLE LANGUAGE sql;

SET search_path TO DEFAULT;

""".format(funcs_lrmversion) 

    